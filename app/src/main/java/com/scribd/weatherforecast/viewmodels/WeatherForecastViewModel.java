package com.scribd.weatherforecast.viewmodels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.scribd.weatherforecast.models.Resource;
import com.scribd.weatherforecast.models.WeatherForecastResponse;
import com.scribd.weatherforecast.repositories.WeatherForecastRepository;

/**
 * Created by Sayyam on 2020-01-22.
 */
public class WeatherForecastViewModel extends ViewModel {

    private WeatherForecastRepository weatherForecastRepository = WeatherForecastRepository.getInstance();
    private MutableLiveData<Resource<WeatherForecastResponse>> weatherForecast;

    public LiveData<Resource<WeatherForecastResponse>> getWeatherForecast(String city) {
        if (weatherForecast == null || weatherForecast.getValue() == null || weatherForecast.getValue().status != Resource.Status.SUCCESS) {
            weatherForecast = new MutableLiveData<>();

            loadWeatherForecast(city);
        }

        return weatherForecast;
    }

    private void loadWeatherForecast(String city) {
        weatherForecast = weatherForecastRepository.getWeatherForecast(city);
    }
}
