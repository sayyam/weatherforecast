package com.scribd.weatherforecast.fragments;


import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.scribd.weatherforecast.R;
import com.scribd.weatherforecast.adapters.WeatherForecastAdapter;
import com.scribd.weatherforecast.viewmodels.WeatherForecastViewModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class WeatherForecastFragment extends Fragment {


    private static final String TAG = "WeatherForecastFragment";
    @BindView(R.id.weatherForecastRecyclerView)
    RecyclerView weatherForecastRecyclerView;

    private Context activityContext;

    private ProgressDialog progressDialog;
    private AlertDialog errorAlertDialog;
    private AlertDialog.Builder errorDialogBuilder;
    private WeatherForecastAdapter weatherForecastAdapter = new WeatherForecastAdapter();


    public WeatherForecastFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        activityContext = context;

        progressDialog = new ProgressDialog(activityContext);
        progressDialog.setMessage(getString(R.string.msg_loading));

        errorDialogBuilder = new AlertDialog.Builder(activityContext);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        String city = WeatherForecastFragmentArgs.fromBundle(getArguments()).getCity();
        Log.d(TAG, "Received city:" + city);

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_weather_forecast, container, false);
        ButterKnife.bind(this, view);

        weatherForecastAdapter.updateList(new ArrayList<>());
        weatherForecastRecyclerView.setAdapter(weatherForecastAdapter);

        WeatherForecastViewModel weatherForecastViewModel = ViewModelProviders.of(this).get(WeatherForecastViewModel.class);

        weatherForecastViewModel.getWeatherForecast(city).observe(this, weatherForecastResource -> {

            switch (weatherForecastResource.status) {
                case SUCCESS: {
                    progressDialog.dismiss();

                    Log.d(TAG, "Received Forecast Total:" + weatherForecastResource.data.getCnt().toString());
                    weatherForecastAdapter.updateList(weatherForecastResource.data.getList());
                    weatherForecastAdapter.notifyDataSetChanged();
                }
                break;

                case ERROR: {
                    progressDialog.dismiss();
                    if (null != errorAlertDialog && !errorAlertDialog.isShowing()) {
                        errorAlertDialog.dismiss();
                    }

                    errorDialogBuilder.setMessage(getString(R.string.msg_could_not_load)).setTitle(getString(R.string.title_error))
                            .setCancelable(false)
                            .setPositiveButton(getString(R.string.refresh), (dialog, id) -> weatherForecastViewModel.getWeatherForecast(city))
                            .setNegativeButton(getString(R.string.back), (dialogInterface, i) -> Navigation.findNavController(view).popBackStack());

                    errorAlertDialog = errorDialogBuilder.create();
                    errorAlertDialog.show();
                }
                break;

                case LOADING: {
                    if (!progressDialog.isShowing())
                        progressDialog.show();
                }
                break;
            }
        });

        weatherForecastRecyclerView = view.findViewById(R.id.weatherForecastRecyclerView);

        LinearLayoutManager llm = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);

        weatherForecastRecyclerView.setLayoutManager(llm);

        return view;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        activityContext = null;
    }

}
