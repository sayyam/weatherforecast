package com.scribd.weatherforecast.fragments;


import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.fragment.app.Fragment;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;

import com.scribd.weatherforecast.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class SearchCityFragment extends Fragment {

    private static final String TAG = "SearchCityFragment";

    @BindView(R.id.cityNameEditText)
    EditText cityNameEditText;

    @BindView(R.id.submitButton)
    Button submitButton;


    public SearchCityFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_search_city, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @OnClick(R.id.submitButton)
    public void submitCity(View view) {
        String city = cityNameEditText.getText().toString();
        Log.d(TAG, "Entered City:" + city);

        NavDirections action =
                SearchCityFragmentDirections
                        .actionSubmitCity(city);
        Navigation.findNavController(view).navigate(action);

    }

}
