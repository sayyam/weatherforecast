package com.scribd.weatherforecast.adapters;

import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.scribd.weatherforecast.R;
import com.scribd.weatherforecast.utils.DateUtils;
import com.scribd.weatherforecast.utils.TemperatureUtils;

import java.util.List;

/**
 * Created by Sayyam on 2020-01-22.
 */
public class WeatherForecastAdapter extends RecyclerView.Adapter<WeatherForecastAdapter.WeatherViewHolder> {

    List<com.scribd.weatherforecast.models.List> weatherForecastList;

    public void updateList(List<com.scribd.weatherforecast.models.List> weatherForecastList) {
        this.weatherForecastList = weatherForecastList;
    }

    @NonNull
    @Override
    public WeatherViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = View.inflate(parent.getContext(), R.layout.item_weather_forecast, null);

        WeatherViewHolder viewHolder = new WeatherViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull WeatherViewHolder holder, int position) {

        com.scribd.weatherforecast.models.List forecast = weatherForecastList.get(position);
        holder.currentTemperatureTextView.setText(String.format("%.0f°", TemperatureUtils.kelvinToCelsius(forecast.getMain().getTemp())));
        holder.minTemperatureTextView.setText(String.format("min: %.0f°", TemperatureUtils.kelvinToCelsius(forecast.getMain().getTempMin())));
        holder.maxTemperatureTextView.setText(String.format("  max:%.0f°", TemperatureUtils.kelvinToCelsius(forecast.getMain().getTempMax())));
        holder.weatherTextView.setText("Weather: " + forecast.getWeather().get(0).getDescription());
        holder.dateTextView.setText(DateUtils.getReadableDateFromServerDate(forecast.getDtTxt()));

    }

    /**
     * Returns the total number of items in the data set held by the adapter.
     *
     * @return The total number of items in this adapter.
     */
    @Override
    public int getItemCount() {
        return weatherForecastList.size();
    }

    class WeatherViewHolder extends RecyclerView.ViewHolder {

        TextView currentTemperatureTextView;
        TextView minTemperatureTextView;
        TextView maxTemperatureTextView;
        TextView weatherTextView;
        TextView dateTextView;

        public WeatherViewHolder(@NonNull View itemView) {

            super(itemView);
            currentTemperatureTextView = itemView.findViewById(R.id.currentTemperatureTextView);
            minTemperatureTextView = itemView.findViewById(R.id.minTemperatureTextView);
            maxTemperatureTextView = itemView.findViewById(R.id.maxTemperatureTextView);
            weatherTextView = itemView.findViewById(R.id.weatherTextView);
            dateTextView = itemView.findViewById(R.id.dateTextView);
        }
    }
}
