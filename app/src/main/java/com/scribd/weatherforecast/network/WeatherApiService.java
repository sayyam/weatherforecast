package com.scribd.weatherforecast.network;

import com.scribd.weatherforecast.models.WeatherForecastResponse;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Sayyam on 2020-01-22.
 */
public interface WeatherApiService {
    String API_BASE_URL = "https://samples.openweathermap.org/data/2.5/";
    String OPEN_WEATHER_API_KEY = "ade9cb37e9208134e65b0758bbf170dd";

    @GET("forecast")
    Call<WeatherForecastResponse> getWeatherForecast(@Query("q") String numberOfResults);

    class Creator {

        public static WeatherApiService create() {

            //Request interceptor to add appid for each request
            OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
            httpClient.addInterceptor(chain -> {
                Request original = chain.request();

                HttpUrl url = original.url()
                        .newBuilder()
                        .addQueryParameter("appid", OPEN_WEATHER_API_KEY).build();

                Request.Builder requestBuilder = original.newBuilder()
                        .url(url);

                Request request = requestBuilder.build();
                return chain.proceed(request);
            });

            OkHttpClient client = httpClient.build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(API_BASE_URL).client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            return retrofit.create(WeatherApiService.class);
        }
    }
}

