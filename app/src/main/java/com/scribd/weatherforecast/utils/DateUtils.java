package com.scribd.weatherforecast.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Sayyam on 2020-01-22.
 */
public class DateUtils {

    private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//2017-02-16 12:00:00
    private static SimpleDateFormat prettyYearFormat = new SimpleDateFormat("dd MMM yyyy");//2016


    public static String getReadableDateFromServerDate(String dateTimeString) {

        try {
            Date date = simpleDateFormat.parse(dateTimeString);
            return prettyYearFormat.format(date);

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }
}
