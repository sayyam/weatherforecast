package com.scribd.weatherforecast.utils;

/**
 * Created by Sayyam on 2020-01-22.
 */
public class TemperatureUtils {

    public static Double kelvinToCelsius(double kelvins) {
        return kelvins - 273.0;
    }
}
