package com.scribd.weatherforecast.repositories;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.scribd.weatherforecast.models.Resource;
import com.scribd.weatherforecast.models.WeatherForecastResponse;
import com.scribd.weatherforecast.network.WeatherApiService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Sayyam on 2020-01-22.
 */
public class WeatherForecastRepository {

    private WeatherForecastRepository() {
    }

    private static final WeatherForecastRepository ourInstance = new WeatherForecastRepository();

    public static WeatherForecastRepository getInstance() {
        return ourInstance;
    }

    private WeatherApiService weatherApiService = WeatherApiService.Creator.create();

    MutableLiveData<Resource<WeatherForecastResponse>> weatherForecast = new MutableLiveData<>();

    public MutableLiveData<Resource<WeatherForecastResponse>> getWeatherForecast(String city) {

        weatherForecast.postValue(Resource.loading());

        weatherApiService.getWeatherForecast(city).enqueue(new Callback<WeatherForecastResponse>() {
            @Override
            public void onResponse(Call<WeatherForecastResponse> call, Response<WeatherForecastResponse> response) {
                Log.e("WeatherForecastRepo", "onResponse");
                weatherForecast.postValue(Resource.success(response.body()));
            }

            @Override
            public void onFailure(Call<WeatherForecastResponse> call, Throwable t) {
                Log.e("WeatherForecastRepo", "onFailure");
                weatherForecast.postValue(Resource.error(t.getMessage()));
            }
        });

        return weatherForecast;
    }
}
